# AnyKernel3 Ramdisk Mod Script
# osm0sis @ xda-developers

## AnyKernel setup
# begin properties
properties() { '
kernel.string=zest kernel. Making the sour sweet ;)
do.devicecheck=1
do.modules=1
do.cleanup=1
do.cleanuponabort=1
device.name1=bullhead
device.name2=
device.name3=
device.name4=
device.name5=
supported.versions=8.1-9
'; } # end properties

# shell variables
block=auto;
is_slot_device=0;
ramdisk_compression=auto;


## AnyKernel methods (DO NOT CHANGE)
# import patching functions/variables - see for reference
. tools/ak3-core.sh;


## AnyKernel file attributes
# set permissions/ownership for included ramdisk files
chmod -R 750 $ramdisk/*;
chmod -R 755 $ramdisk/sbin/*;
chown -R root:root $ramdisk/*;


## AnyKernel install
dump_boot;

# begin ramdisk changes

# init.rc
remove_line init.rc "init.performance_profiles.rc";
insert_line init.rc "init.zest.rc" after "ro.zygote" "import /init.zest.rc";

# init.bullhead.rc
remove_line init.bullhead.rc "init.elementalx.rc";
remove_line init.bullhead.rc "init.franco.rc";
remove_line init.bullhead.rc "init.performance_profiles.rc";
remove_line init.bullhead.rc "init.zest";
replace_string init.bullhead.rc "#    verity_load_state" "    verity_load_state" "#    verity_load_state";
replace_string init.bullhead.rc "#    verity_update_state" "    verity_update_state" "#    verity_update_state";
replace_section init.bullhead.rc "service atfwd" " " "service atfwd /system/bin/ATFWD-daemon\n    disabled\n    class late_start\n    user system\n    group system radio\n";
replace_section init.bullhead.rc "service thermal-engine" " " "service thermal-engine /system/bin/thermal-engine\n    class main\n    user root\n    socket thermal-send-client stream 0666 system system\n    socket thermal-recv-client stream 0660 system system\n    socket thermal-recv-passive-client stream 0666 system system\n    writepid /dev/cpuset/system-background/tasks\n    group root\n";

# fstab.bullhead
remove_line fstab.bullhead "cache          f2fs";
patch_fstab /system/vendor/etc/fstab.qcom data ext4 options "forcefdeorfbe=/dev/block/platform/soc.0/f9824900.sdhci/by-name/metadata" "fileencryption";
patch_fstab /system/vendor/etc/fstab.qcom data ext4 options "encryptable=/dev/block/platform/soc.0/f9824900.sdhci/by-name/metadata" "fileencryption";
remove_line fstab.bullhead "data           f2fs";
insert_line fstab.bullhead "data           f2fs" after "data           ext4" "/dev/block/platform/soc.0/f9824900.sdhci/by-name/userdata     /data           f2fs    rw,discard,nosuid,nodev,noatime,nodiratime,nobarrier,inline_xattr,inline_data    wait,check,formattable,fileencryption";
patch_fstab fstab.bullhead none swap flags "zramsize=533413200" "zramsize=1066826400";

# thermal-engine-8992.conf
mount -o rw,remount -t auto /system;
chmod "0755" "0644" "/tmp/anykernel/custom/h3at1ng";
cp -f /tmp/anykernel/custom/h3at1ng /system/etc/thermal-engine-8992.conf;
mount -o ro,remount -t auto /system;

# end ramdisk changes

write_boot;
## end install


# shell variables
block=/dev/block/platform/soc.0/f9824900.sdhci/by-name/recovery;
is_slot_device=0;
ramdisk_compression=auto;

# reset for recovery patching
reset_ak;


## AnyKernel recovery install

#ui_print "Patching recovery partition...";

split_boot;

# fix cmdline options
patch_cmdline "boot_cpus" "boot_cpus=0-3";
patch_cmdline "maxcpus" "maxcpus=4";

flash_boot;

## end install