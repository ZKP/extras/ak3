#!/system/bin/sh

################################################################################

function write() {
    echo -n $2 > $1
}

function copy() {
    cat $1 > $2
}

function get-set-forall() {
    for f in $1 ; do
        cat $f
        write $f $2
    done
}

function writepid_sbg() {
    until [ ! "$1" ]; do
        echo -n $1 > /dev/cpuset/system-background/tasks
        shift
    done
}

################################################################################

function kaliuuuidds() {
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor interactive
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq 384000
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq 1440000
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/go_hispeed_load 93
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/above_hispeed_delay 0  600000:19000 787200:20000 960000:24000 1248000:38000
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/timer_rate 50000
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/hispeed_freq 600000
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/timer_slack 380000
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/target_loads 29 384000:88 600000:90 787200:92 960000:93 1248000:98
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/min_sample_time 60000
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/boost 0
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/align_windows 1
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/use_migration_notif 1
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/use_sched_load 0
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/max_freq_hysteresis 0
    write /sys/devices/system/cpu/cpu0/cpufreq/interactive/boostpulse_duration 0
    write /sys/devices/system/cpu/cpu4/cpufreq/scaling_governor interactive
    write /sys/devices/system/cpu/cpu4/cpufreq/scaling_min_freq 384000
    write /sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq 1824000
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/go_hispeed_load 150
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/above_hispeed_delay 20000 960000:60000 1248000:30000
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/timer_rate 60000
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/hispeed_freq 960000
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/timer_slack 380000
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/target_loads 98
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/min_sample_time 60000
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/boost 0
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/align_windows 1
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/use_migration_notif 1
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/use_sched_load 0
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/max_freq_hysteresis 0
    write /sys/devices/system/cpu/cpu4/cpufreq/interactive/boostpulse_duration 0
    write /sys/module/cpu_boost/parameters/input_boost_freq 0:600000 1:600000 2:600000 3:600000 4:960000 5:960000
    write /sys/module/cpu_boost/parameters/sync_threshold 1248000
    write /sys/module/cpu_boost/parameters/boost_ms 40
    write /sys/module/cpu_boost/parameters/migration_load_threshold 15
    write /sys/module/cpu_boost/parameters/load_based_syncs Y
    write /sys/module/cpu_boost/parameters/shed_boost_on_input N
    write /sys/module/cpu_boost/parameters/input_boost_ms 300
    write /sys/module/cpu_boost/parameters/input_boost_enabled 1
    write /sys/module/msm_thermal/core_control/enabled 0
    write /sys/module/msm_thermal/parameters/enabled Y
    write /proc/sys/kernel/sched_boost 0
    write /proc/sys/kernel/sched_freq_inc_notify 3000000
    write /proc/sys/kernel/sched_init_task_load 40
    write /proc/sys/kernel/sched_prefer_sync_wakee_to_waker 1
    write /proc/sys/kernel/sched_spill_load 90
}

LOGCAT=`pidof logcat`
RMT_STORAGE=`pidof rmt_storage`
QMUXD=`pidof qmuxd`
QTI=`pidof qti`
NETMGRD=`pidof netmgrd`
THERMAL-ENGINE=`pidof thermal-engine`
WPA_SUPPLICANT=`pidof wpa_supplicant`
LOC_LAUNCHER=`pidof loc_launcher`
CNSS-DAEMON=`pidof cnss-daemon`
QSEECOMD=`pidof qseecomd`
TIME_DAEMON=`pidof time_daemon`  CND=`pidof cnd`
IMSQMIDAEMON=`pidof imsqmidaemon`
IMSDATADAEMON=`pidof imsdatadaemon`
writepid_sbg $LOGCAT
writepid_sbg $RMT_STORAGE
writepid_sbg $QMUXD
writepid_sbg $QTI
writepid_sbg $NETMGRD
writepid_sbg $THERMAL-ENGINE
writepid_sbg $WPA_SUPPLICANT
writepid_sbg $LOC_LAUNCHER
writepid_sbg $CNSS-DAEMON
writepid_sbg $QSEECOMD
writepid_sbg $TIME_DAEMON
writepid_sbg $CND
writepid_sbg $IMSQMIDAEMON
writepid_sbg $IMSDATADAEMON
kaliuuuidds
{

sleep 20
kaliuuuidds

}&
